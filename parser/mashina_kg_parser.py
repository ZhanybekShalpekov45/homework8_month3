from pprint import pprint

import requests
from parsel import Selector

MAIN_URL = "https://www.mashina.kg"


def get_html(url: str):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception("Страница не загружается")


def parse_html(html: str):
    selector = Selector(text=html)
    return selector


def get_title(selector: Selector):
    return selector.css("title::text").get()


def get_cars(selector: Selector):
    return selector.css(".search-results-table .list-item").getall()


def clean_text(text: str):
    if text is None:
        return ""

    text = text.strip()
    text = text.replace("\n", " ").replace("\r", " ").replace("\t", " ")

    return text


def clean_text_list(lst: list):
    if lst is None:
        return ""

    big_list = []
    for txt in lst:
        big_list.append(clean_text(txt))

    return "".join(big_list)


def get_cars_info(selector: Selector):
    cars_info = []
    for car in selector.css(".search-results-table .list-item"):
        car_info = {}
        car_info["title"] = clean_text(car.css("h2.name::text").get())
        car_info["url"] = f"{MAIN_URL}{car.css('a').attrib.get('href')}"
        car_info["price_usd"] = clean_text(
            car.css(".block.price strong::text").get()
        )
        car_info["color"] = car.css(".color-icon").attrib.get("title")
        car_info["description"] = clean_text_list(
            car.css(".item-info-wrapper *::text").getall()
        )

        cars_info.append(car_info)

    return cars_info


def check_error_page(selector: Selector):
    return selector.css(".no-results").get() is not None


def main():
    page = 1
    while True:
        html = get_html(f"{MAIN_URL}/search/all/?page={page}")
        # pprint(html[:400])
        selector = parse_html(html)

        if check_error_page(selector):
            break
        # pprint(get_title(selector))
        # pprint(len(get_cars(selector)))
        pprint(get_cars_info(selector))

        print("========", page)

        page += 1


if __name__ == "__main__":
    main()
