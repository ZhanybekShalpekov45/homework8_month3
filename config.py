from os import getenv

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from dotenv import load_dotenv

load_dotenv()
storage = MemoryStorage()
bot = Bot(token=getenv('BOT_TOKEN'))
dp = Dispatcher(bot, storage=storage)
scheduler = AsyncIOScheduler()
ADMIN = [1036061654]
