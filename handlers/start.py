from aiogram import types

from handlers.constants import HELLO_TEXT


# @dp.message_handler(commands=["start"])
async def start(message: types.Message):
    # await message.answer("Привет")
    print(f"{message.from_user=}")
    # await message.reply(f"Привет, {message.from_user.username}")

    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton(
            "Наш сайт",
            url="https://geektech.edu.kg/"
        ),
        types.InlineKeyboardButton(
            "О нас",
            callback_data='about'
        )
    )
    kb.add(types.InlineKeyboardButton(
        "Истории успеха",
        callback_data='success_story')
    )

    await message.reply(HELLO_TEXT, reply_markup=kb)


# @dp.message_handler(commands=["pic"])
async def send_picture(message: types.Message):
    """Отправляет картинку пользователю"""
    # Doc String
    with open('images/cat.jpg', 'rb') as photo:
        await message.answer_photo(
            photo, 
            caption="Кошка")


# @dp.message_handler()
async def echo(message: types.Message):
    """Эхо, функция, которая отправляет пользоватео"""
    await message.answer(message.text)
