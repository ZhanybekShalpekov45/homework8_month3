from pprint import pprint

from aiogram import types

from db.queries import get_frontend_teachers, get_teachers


async def education(message: types.Message):
    kb = types.ReplyKeyboardMarkup()
    kb.add(
        types.KeyboardButton("Front End"),
        types.KeyboardButton("Back End"),
        types.KeyboardButton("Всё"),
    )
    await message.answer(
        "Выберите интересующее вас направление:",
        reply_markup=kb
    )


async def education_all(messages: types.Message):
    teachers = get_teachers()
    pprint(teachers)


async def show_front_end(message: types.Message):
    teachers = get_frontend_teachers()
    await message.answer("Самое лучшее направление")
    for t in teachers:
        await message.answer()
