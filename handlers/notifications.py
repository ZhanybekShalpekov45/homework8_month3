from datetime import datetime

from aiogram import types

from config import bot, scheduler


async def handle_notify(message: types.Message):
    user_id = str(message.from_user.id)
    if scheduler.get_job(user_id) is not None:
        await message.answer("Вы уже установили нопоминание")
    else:
        # задачи по интервалу
        # scheduler.add_job(
        #     send_notification,
        #     "interval",
        #     seconds=7,
        #     id=user_id,
        #     kwargs={'user_id': message.from_user.id}
        # )

        # задача в определенную дату  
        scheduler.add_job(
            send_notification,
            "date",
            run_date=datetime(2023, 7, 27, 17, 21),
            kwargs={'user_id': message.from_user.id}
        )

        # scheduler.add_job(
        #     send_notification,
        #     "cron",
        #     day="25,30",
        #     hour=17,
        #     minute=32,
        #     second=20,
        #     kwargs={'user_id': message.from_user.id}
        # )
        await message.answer("Принято!")


async def send_notification(user_id: int):
    await bot.send_message(
        chat_id=user_id,
        text="Привет, мой друг"
    )
