from aiogram import types

from config import bot, ADMIN

# async def check_author_is_admin(message: types.Message):
#     member_type = await message.chat.get_member(message.from_user.id)
#     pprint(member_type)
#     return not member_type["status"] != "member"
#
#
# async def example(message: types.Message):
#     print(f"{message.chat.type=}")
#     print(f"{message.reply_to_message=}")
#
#     if message.chat.type != "private":
#         await message.answer("Это сообщение было отправлерно не в ЛС")
#     else:
#         await message.answer("Этот бот работает только в группе")


bad_words = ("мамбет", "лох")
warnings = {}


async def catch_bad_words(message: types.Message):
    """Проверяет сообщение на наличие запрещенных слов"""
    if message.chat.type != "private":
        user_id = message.from_user.id

        if user_id not in warnings:
            warnings[user_id] = 0

        for word in bad_words:
            if word in message.text.lower().replace(" ", ""):
                if warnings[user_id] == 0:
                    await message.answer("Тут запрещены маты!")
                else:
                    await message.answer(f"Не матерись @{message.from_user.first_name},"
                                         f"сам ты {word}!")
                warnings[user_id] += 1

                await message.delete()

                if warnings[user_id] >= 2:
                    await message.answer(f"Пользователь @{message.from_user.username} был забанен за маты.")
                    await bot.kick_chat_member(message.chat.id,
                                               message.from_user.id)

                    break

    if message.from_user.id not in ADMIN:
        await message.answer("Ты,не админ!")
        await bot.kick_chat_member(message.chat.id,
                                           message.from_user.id
                                           )




# async def pin_message_in_group(message: types.Message):
#     if message.chat.type != "private":
#         is_admin = await check_author_is_admin(message)
#         reply = message.reply_to_message
#         if reply is not None and is_admin:
#             await reply.pin()
#
#
# async def ban_group_user(message: types.Message):
#     if message.chat.type != "private":
#         reply = message.reply_to_message
#         # is_admin = check_author_is_admin(reply)
#         if reply is not None:
#             await message.bot.ban_chat_member(
#                 chat_id=message.chat.id, user_id=reply.from_user.id
#             )
