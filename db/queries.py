import sqlite3
from pathlib import Path


def init_db():
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH/DB_NAME)
    cursor = db.cursor()


def create_tables():
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS teachers(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            field TEXT,
            age INTEGER
        )
        """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS survey_results(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            age INTEGER,
            gender TEXT
        )
    """)
    db.commit()


def populate_tables():
    cursor.execute("""
        INSERT INTO teachers (name, field, age) 
        VALUES  ('igor', 'backend', 35),
                ('igor', 'frontend', 35)
    """)
    db.commit()


def save_survey(data):
    cursor.execute(
        """
        INSERT INTO survey_results (name, age, gender) 
        VALUES (:name, :age, :gender)
        """,
        {'name': data['name'], 
            'age': data['age'],
            'gender': data['gender']}
    )
    db.commit()


def drop_tables():
    cursor.execute("""
        DROP TABLE IF EXISTS teachers
    """)
    db.commit()


def get_teachers():
    teachers = cursor.execute("""
        SELECT * FROM teachers;
    """)
    return teachers.fetchall()


def get_frontend_teachers():
    teachers = cursor.execute("""
        SELECT * FROM teachers WHERE teachers.field = 'frontend';
    """)
    return teachers.fetchall()


def delete_teacher():
    """DELETE * FROM teachers WHERE name = 'Igor' """


if __name__ == '__main__':
    init_db()
    drop_tables()
    create_tables()
    populate_tables()
