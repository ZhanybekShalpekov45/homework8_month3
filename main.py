import logging

from aiogram import executor
from aiogram.dispatcher.filters import Text

from config import dp, scheduler
from db.queries import create_tables, drop_tables, init_db, populate_tables
from handlers.about_us import about_us
from handlers.education import education, education_all
from handlers.group_admin import catch_bad_words
from handlers.notifications import handle_notify
from handlers.start import echo, send_picture, start
from handlers.user_form import register_survey_handlers


async def bot_start(_):
    init_db()
    drop_tables()
    create_tables()
    populate_tables()


# PEP 8
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    # dp.register_callback_query_handler(about_us, Text('about'))
    # dp.register_callback_query_handler(about_us, lambda cb: cb.data == "about")

    # dp.register_message_handler(education, commands=["edu"])
    # dp.register_message_handler(show_back_end, Text("Back End"))
    # dp.register_message_handler(education_all, Text("Всё"))

    # dp.register_message_handler(send_picture, commands=["pic"])
    # dp.register_message_handler(start, commands=["start"])

    # команды для групп
    # dp.register_message_handler(
    #     pin_message_in_group, commands="pin", commands_prefix="!"
    # )
    # dp.register_message_handler(ban_group_user,
    #                             commands="ban", commands_prefix="!")

    # планировщик
    # dp.register_message_handler(handle_notify, commands=["notify"])

    # опросник
    # register_survey_handlers(dp)

    # в самом конце
    dp.register_message_handler(catch_bad_words)

    scheduler.start()
    executor.start_polling(dp, on_startup=bot_start, skip_updates=True)
